#!/bin/bash

# A helpful file to source in bash or zsh
# tested on linux and from isilon
#####################################
#needed external programs

# normal unix commands such as
# date dig grep awk
# maybe tr

# less common programs such as
# kinit ldapsearch ldapmodify


#####################################
# adds commands

# ad_disable <username>
# To disable an ldap account <username>

# ad_enable <username>
# To enable an ldap account <username>

# ad_renew <username>
# To renew an account 6 months from its current expire date
# if adding 6 months the new date is less than 6 months
# keep adding 6 months till it is greater than 5 and less that 12

# ad_modify <user> <ldap symbol> <old value> <new value>
# an easy way to use ldapmodify to change a symbol's value
#

# ldapcn <Extra Base Stuff> <ldap symbol>
# an easyer way to user ldapsearch to pull a symbol
# ex:
#  ldapcn "ypservers.ypServ30.RpcServices.System" cn


# ldapu <username> <ldap symbol>
# an easyer way to user ldapsearch to pull a symbol
# ex:
#  ldapu username mail
#    username@subdomain.domain

# w2s <100-nanoseconds>
# converts 100-nanoseconds to seconds

# get a dn from ldapu and use it in ldapsearch
# ldapsearch $(ldapargs) -b "$(ldapu $USER dn)"
# maybe useful in a group membership situation

# get an ObjectGUID and use it in ldapsearch
# ldapsearch $(ldapargs) -b "<GUID=$(base64toGuid $(ldapu $USER objectGUID))>"
# if you have the GUID

# get an ObjectSID and use it in ldap
# ldap "(ObjectSid=$(base64toSid $(ldapu $USER objectSid)))"


if [ "${timestamp}" = "" ];then
 timestamp=$(date +%s)
fi

if [ "$(date -h 2>&1|grep "f fmt date")" = "" ];then
function h2u()
{
 date +%s -d "$1"
}
else
function h2u()
{
 local d
 d=$1
 date -f "%y%m%d%H%M.%S" "${d:6:2}${d:0:2}${d:3:2}${d:9:2}${d:12:2}.${d:15:2}" +%s
}
fi

function nth()
{
 local n
 n=0;
 echo -e "$1" |while IFS= read line
 do
  n=$((${n}+1));
  if [ ${n} = $2 ];then
   echo -e "${line}"
   break
  fi
 done
}

function ad_switch()
{
 local domains
 local count
 count=0
 if [ -f /etc/resolv.conf ];then
  domains=$(awk '$1=="search" {for (i=2;i<NF+1;i++) print $i}' /etc/resolv.conf|sed 's/\.$//')
  if [ "${domains}" != "" ];then
   local domain
   for domain in ${domains};
   do
    count=$((${count}+1))
    if [ "${count}" = "1" ];then
     echo "Press 1 and Enter or just Enter for [${domain}]"
    else
     echo "Press ${count} and Enter for [${domain}]"
    fi
   done
   echo "${count} to choose from"
   echo -n "or "
  fi
 fi
 echo "Type in your ad domain name and press Enter. ex:myad.domain.com"
 read ad_domain
 local re
 re='^[0-9]+$'
 if [[ ${ad_domain} =~ $re ]] && [ ${ad_domain} -le ${count} ] && [ ${count} -gt 0 ]; then
  ad_domain=$(nth "${domains}" ${ad_domain})
 else
  if [ ${count} -gt 0 ] && [ "${ad_domain}" = "" ]; then
   ad_domain=$(nth "${domains}" 1)
  fi
 fi
 echo "Setting ad_domain variable to [${ad_domain}]"
 echo "Type the user you wish to use for domain ${ad_domain}. or Press Enter to use [$USER]"
 kerberos_ticket_expires=""
 read ad_user
 if [ "${ad_user}" = "" ];then
  ad_user="$USER"
 fi
}

if [ "${ad_user}" = "" ] || [ "${ad_domain}" = "" ];then
 echo "If you add ad_user and ad_domain to your ~/.bashrc file you can avoid it asking from a source"
 ad_switch
fi

function upper_builder()
{
 if [ "$(upper_test="a";echo ${upper_test^^} 2>/dev/null)" = "A" ];then
 echo "bash"
 function upper()
 {
  echo -e "${1^^}"
 }
 else
 echo "tr"
 function upper()
 {
  echo -e "$1" | tr /a-z/ /A-Z/
 }
 fi
}

upper_builder 2>/dev/null

function ad_key_check()
{
 local key
 local expired
 local timestamp
 timestamp=$(date +%s)
 if [ "${kerberos_ticket_expires}" = "" ] || [ ${kerberos_ticket_expires} -le ${timestamp} ];then
  expired="TRUE"
  key=$(nth "$(echo $(klist -A 2>/dev/null)|sed "s/Ticket cache: /\n/g"|sed "s/.*Default principal: //g"|sed "s/ .*renew until//g"|grep -i "${ad_domain}")" 1)
  if [ "${key}" != "" ];then
   if [ "${key:0:${#ad_user}}" = "${ad_user}" ];then
    local expires
    kerberos_ticket_expires=$(h2u "$(echo ${key}|awk '{print $2 " " $3}')")
    if [ ${kerberos_ticket_expires} -gt ${timestamp} ];then
     if [ "${quiet}" = "no" ];then
      echo "Still have a valid key ${key}" 1>&2
     fi
     expired="FALSE"
    fi
   fi
  fi
 fi
 if [ "${expired}" = "TRUE" ];then
  echo -e "\n\nType in your AD password for ${ad_user}@$(upper ${ad_domain})" 1>&2
  local PASSWORD
  read -s PASSWORD
  echo ${PASSWORD}|kinit "${ad_user}@$(upper ${ad_domain})" 1>&2
  unset PASSWORD
 fi
}



function ldapuri()
{
 if [ "${LDAPURI}" != "" ] && [ "$1" = "" ];then
  echo -e "${LDAPURI}"
 else
  local a
  a="+noedns +short"
  #you may need ldaps and 636 instead of 389 for your port
  echo "ldap://$(eval dig ${a} -x $(nth "$(eval dig ${a} "${ad_domain}")" 1)):389"
 fi
}

function ldapargs()
{
 echo "-H $(ldapuri) -U ${ad_user} -Q"
}

if [ "$(ldapsearch --help 2>&1|grep ldif-wrap)" != "" ];then
echo "turning off ldif-wrap"
function searchargs()
{
 # ${ad_domain//./,dc=} replaces . with ,dc= in the d variable
 echo "-LLL -b $1dc=${ad_domain//./,dc=} -o ldif-wrap=no"
}
else
echo "does not have ldif-wrap"
function searchargs()
{
 # ${ad_domain//./,dc=} replaces . with ,dc= in the d variable
 echo "-LLL -b $1dc=${ad_domain//./,dc=}"
}
fi

function ldap()
{
 ad_key_check
 local item
 local line
 local out
 ldapsearch $(ldapargs) $(searchargs) "$@"
}

function ldap_users()
{
 local name
 name=$1
 shift
 ldap "(&(objectclass=person)(&(name=${name})))" "$@"
}

function ldap_groups()
{
 local name
 name=$1
 shift
 ldap "(&(objectclass=group)(&(name=${name})))" "$@"
}

function ldapu()
{
 ad_key_check
 local item
 local line
 local out
 item="$2"
 if [ "${item}" = "" ];then
  ldapsearch $(ldapargs) $(searchargs) "(sAMAccountName=$1)"
 else
#  echo "ldapsearch $(ldapargs) $(searchargs) \"(sAMAccountName=$1)\" ${item}"
  out=$(ldapsearch $(ldapargs) $(searchargs) "(sAMAccountName=$1)" "${item}")
  echo -e "${out}" | grep "^${item}:" | sed "s/^.*: //"
 fi
}

function ldapg()
{
local ad_uac
local active
for user in $(wbinfo -u);do
 ad_uac=$(ldapu ${user} userAccountControl)
 if [ "$((${ad_uac}&2))" = "2" ];then
  active="disabled"
 else
  active="active"
 fi
 if [ "$(ldapu "$user" memberOf|grep -i $1)" = "" ]; then
 echo "Vaccant from $1:${active}:${user}"
 else
 echo "IN $1:${active}:${user}"
 fi
done
}


function ldapcn()
{
 ad_key_check
 local cn
 cn=$1
 cn="cn=${cn//./,cn=},"
 shift;
 ldapsearch $(ldapargs) $(searchargs ${cn}) "$@"
}

function w2s()
{
 if [ ${#1} -gt 7 ] && [ "${1:0:1}" != "-" ] || [ ${#1} -gt 8 ];then
  echo ${1::-7}
 else
  echo -11644473600;
 fi
}

function w2u()
{
# w2u 131032318800490007
# 1458758280
#
# 11644473600 is the windows offset by picking Jan 1 1601 as the start
# vs unix picking Jan 1 1970 as the start
 if [ ${#1} -gt 7 ] && [ "${1:0:1}" != "-" ] || [ ${#1} -gt 8 ];then
  echo $((${1::-7} - 11644473600));
 else
  echo -11644473600;
 fi
}

function w2h()
{
 u2h $(w2u "$1")
}

function z2u()
{
 local ymdd
 # z2u 20170510090000.0Z
 # 1494406800
 if [ ${#1} -gt 13 ];then
  # year month day digits
  ymdd=$((${#1}-9))
  date +%s -ud "${1:0:${ymdd}} ${1:${ymdd}:4}" 2>/dev/null||echo 0;
 else
  echo 0;
 fi
}

function z2h()
{
 u2h $(z2u "$1")
}

if [ "$(date -h 2>&1|grep "f fmt date")" = "" ];then
function u2h()
{
 if [ "$1" = "-11644473600" ];then echo "Never"; else date "+%m/%d/%Y %H:%M:%S" -d @$1; fi
}
else
function u2h()
{
 if [ "$1" = "-11644473600" ];then echo "Never"; else date -f "%s" "$1" "+%m/%d/%Y %H:%M:%S"; fi
}
fi

if [ $((-2147483648-1)) -lt 0 ];then
echo "64 bit"
function greater()
{
 local greatest
 greatest=-9223372036854775808;
 while [ $# -gt 0 ];do 
  if [ $1 -gt ${greatest} ];then greatest=$1;fi;shift;
 done;
 echo ${greatest}
}
else
echo "32 bit"
function greater()
{
 local greatest
 greatest=-2147483648
 while [ $# -gt 0 ];do 
  if [ $1 -gt ${greatest} ];then greatest=$1;fi;shift;
 done;
 echo ${greatest}
}
fi


function ad_modify()
{
 local dn
 dn=$(ldapu $1 dn)
 ldapmodify $(ldapargs) <<MODIFY_TAG
dn:${dn}
changetype: modify
replace: $2
$2:$3
-
MODIFY_TAG
}

function ad_replace()
{
 local dn
 dn=$(ldapu $1 dn)
 ldapmodify $(ldapargs) <<MODIFY_TAG
dn:${dn}
changetype: modify
delete: $2
$2:$3
-
add: $2
$2:$4
-
MODIFY_TAG
}

function ad_add()
{
 local dn
 dn=$(ldapu $1 dn)
 ldapmodify $(ldapargs) <<MODIFY_TAG
dn:${dn}
changetype: modify
add: $2
$2:$3
-
MODIFY_TAG
}


function ad_renew()
{
local exp
local new_exp
local month_diff
while [ "$1" != "" ];do
 exp=$(ldapu $1 accountExpires)
 new_exp=${exp}
 month_diff=0
 echo "   user:$1"
 echo "    exp:${exp}"
 if [ "${#exp}" -gt 7 ] && [ "${exp}" != "9223372036854775807" ];then
  while [ ${month_diff} -lt 5 ];do
   #2592000=30*24*3600 approx seconds in a month
   #15552000=6*2592000 approx seconds in 6 months using 30 days in month
   new_exp="$(($(w2s ${new_exp})+15552000))0000000"
   month_diff=$((($(w2u ${new_exp})-${timestamp})/(2592000)))
  done
  echo "new_exp:${new_exp}"
  echo "months out set to ${month_diff}"
  if [ ${month_diff} -gt 11 ];then
   echo "Already renewed"
  else
   ad_replace "$1" "accountExpires" "${exp}" "${new_exp}"
  fi
 else
  echo "Account does not expire."
 fi
 shift
done
echo "No more"
}

function ad_chsh()
{
 local newsh
 local shuser
 if [ "$1" != "" ];then
  if [ "$2" != "" ];then
   shuser=$1
   newsh=$2
  else
   shuser=${USER}
   newsh=$1
  fi
  ad_replace "${shuser}" "loginShell" "$(ldapu ${shuser} loginShell)" "${newsh}"
 else
   echo "$0 <new shell>"
 fi
}

function ad_disable()
{
local ad_uac
local new_uac
while [ "$1" != "" ];do
 ad_uac=$(ldapu $1 userAccountControl)
 echo "   user:$1"
 echo "    uac:${ad_uac}"
 if [ "$((${ad_uac}&2))" = "2" ];then
  echo "Already disabled"
 else
  new_uac=$((${ad_uac}|2))
  echo "new_uac:${new_uac}"
  ad_replace "$1" "userAccountControl" "${ad_uac}" "${new_uac}"
 fi
 shift
done
echo "No more"
}

function ad_enable()
{
local ad_uac
local new_uac
while [ "$1" != "" ];do
 ad_uac=$(ldapu $1 userAccountControl)
 echo "   user:$1"
 echo "    uac:${ad_uac}"
 if [ "$((${ad_uac}&2))" = "2" ];then
  new_uac=$((${ad_uac}^2))
  echo "new_uac:${new_uac}"
  ad_replace "$1" "userAccountControl" "${ad_uac}" "${new_uac}"
 else
  echo "Already enabled"
 fi
 shift
done
}
#!/bin/bash
#[0-25]=[A-Z]
#[26-51]=[a-z]
#[52-61]=[0-9]
#62=+
#63=/
#[=]=place holder to align at bites
#xx= has one place holder haveing 2 bites of data
#x== has two place holders haveing 1 bite of data

#0=000000
#63=111111

#base64  binary       hex
#    AA  000000000000 000
#    AB  000000000001 001
#   ...
#    BA  000001000000 040
#    CA  000010000000 080
#    DA  000011000000 0C0
#    EA  000100000000 100

#    //  111111111111 fff
#    A== 0000         0
#    A=  00000000     00

base64toHex()
{
two_base64_chars_to_3hex()
{
 local char1=${1:0:1}
 local char2=${1:1:1}
 local bite1=""
 local bite2=""
 local bite3=""
 if   [[ "${char1}" =~ [A-D] ]];then      bite1=0;
 elif [[ "${char1}" =~ [E-H] ]];then      bite1=1;
 elif [[ "${char1}" =~ [I-L] ]];then      bite1=2;
 elif [[ "${char1}" =~ [M-P] ]];then      bite1=3;
 elif [[ "${char1}" =~ [Q-T] ]];then      bite1=4;
 elif [[ "${char1}" =~ [U-X] ]];then      bite1=5;
 elif [[ "${char1}" =~ [Y-Za-b] ]];then   bite1=6;
 elif [[ "${char1}" =~ [c-f] ]];then      bite1=7;
 elif [[ "${char1}" =~ [g-j] ]];then      bite1=8;
 elif [[ "${char1}" =~ [k-n] ]];then      bite1=9;
 elif [[ "${char1}" =~ [o-r] ]];then      bite1=a;
 elif [[ "${char1}" =~ [s-v] ]];then      bite1=b;
 elif [[ "${char1}" =~ [w-z] ]];then      bite1=c;
 elif [[ "${char1}" =~ [0-3] ]];then      bite1=d;
 elif [[ "${char1}" =~ [4-7] ]];then      bite1=e;
 else                                     bite1=f;
 fi

 if [ "${char2}" != "=" ] && [ "${char2}" != "" ];then
  if   [[ "${char1}" =~ [AEIMQUYcgkosw048] ]];then
   if   [[ "${char2}" =~ [A-P] ]];then    bite2=0;
   elif [[ "${char2}" =~ [Q-Za-f] ]];then bite2=1;
   elif [[ "${char2}" =~ [g-v] ]];then    bite2=2;
   else                                   bite2=3;
   fi
  elif [[ "${char1}" =~ [BFJNRVZdhlptx159] ]];then
   if   [[ "${char2}" =~ [A-P] ]];then    bite2=4;
   elif [[ "${char2}" =~ [Q-Za-f] ]];then bite2=5;
   elif [[ "${char2}" =~ [g-v] ]];then    bite2=6;
   else                                   bite2=7;
   fi
  elif [[ "${char1}" =~ [CGKOSWaeimquy26+] ]];then
   if   [[ "${char2}" =~ [A-P] ]];then    bite2=8;
   elif [[ "${char2}" =~ [Q-Za-f] ]];then bite2=9;
   elif [[ "${char2}" =~ [g-v] ]];then    bite2=a;
   else                                   bite2=b;
   fi
  else
   if   [[ "${char2}" =~ [A-P] ]];then    bite2=c;
   elif [[ "${char2}" =~ [Q-Za-f] ]];then bite2=d;
   elif [[ "${char2}" =~ [g-v] ]];then    bite2=e;
   else                                   bite2=f;
   fi
  fi

  if   [[ "${char2}" =~ [AQgw] ]];then    bite3=0;
  elif [[ "${char2}" =~ [BRhx] ]];then    bite3=1;
  elif [[ "${char2}" =~ [CSiy] ]];then    bite3=2;
  elif [[ "${char2}" =~ [DTjz] ]];then    bite3=3;
  elif [[ "${char2}" =~ [EUk0] ]];then    bite3=4;
  elif [[ "${char2}" =~ [FVl1] ]];then    bite3=5;
  elif [[ "${char2}" =~ [GWm2] ]];then    bite3=6;
  elif [[ "${char2}" =~ [HXn3] ]];then    bite3=7;
  elif [[ "${char2}" =~ [IYo4] ]];then    bite3=8;
  elif [[ "${char2}" =~ [JZp5] ]];then    bite3=9;
  elif [[ "${char2}" =~ [Kaq6] ]];then    bite3=a;
  elif [[ "${char2}" =~ [Lbr7] ]];then    bite3=b;
  elif [[ "${char2}" =~ [Mcs8] ]];then    bite3=c;
  elif [[ "${char2}" =~ [Ndt9] ]];then    bite3=d;
  elif [[ "${char2}" =~ [Oeu+] ]];then    bite3=e;
  else                                    bite3=f;
  fi
 fi
 echo "${bite1}${bite2}${bite3}"
}

 local word=$1
 local bites=""
 while [ "${word}" != "" ];do
  bites+=$(two_base64_chars_to_3hex ${word:0:2})
  if [ "${word:2:2}" = "==" ];then
   bites=${bites%?}
  else
   bites+=$(two_base64_chars_to_3hex ${word:2:2})
  fi
  word=${word:4}
 done
 echo ${bites}
}
_base64toHex()
{
 echo $(echo $1|base64 -d|hexdump -ve '/1 "%02x"')
}

compare()
{
word=$1
echo "${word}"
_base64toHex $1
echo "${word}"
base64toHex $1
}


hex2base64()
{
six_bin2base64()
{
 local b1=${1:0:1}
 local b2=${1:1:1}
 local b3=${1:2:1}
 local b4=${1:3:1}
 local b5=${1:4:1}
 local b6=${1:5:1}
 if [ "${b1}" = "0" ];then
  if [ "${b2}" = "0" ];then
   if [ "${b3}" = "0" ];then
    if [ "${b4}" = "0" ];then
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo A;else echo B;fi
     else
      if [ "${b6}" = "0" ];then echo C;else echo D;fi
     fi
    else
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo E;else echo F;fi
     else
      if [ "${b6}" = "0" ];then echo G;else echo H;fi
     fi
    fi
   else
    if [ "${b4}" = "0" ];then
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo I;else echo J;fi
     else
      if [ "${b6}" = "0" ];then echo K;else echo L;fi
     fi
    else
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo M;else echo N;fi
     else
      if [ "${b6}" = "0" ];then echo O;else echo P;fi
     fi
    fi
   fi
  else
   if [ "${b3}" = "0" ];then
    if [ "${b4}" = "0" ];then
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo Q;else echo R;fi
     else
      if [ "${b6}" = "0" ];then echo S;else echo T;fi
     fi
    else
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo U;else echo V;fi
     else
      if [ "${b6}" = "0" ];then echo W;else echo X;fi
     fi
    fi
   else
    if [ "${b4}" = "0" ];then
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo Y;else echo Z;fi
     else
      if [ "${b6}" = "0" ];then echo a;else echo b;fi
     fi
    else
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo c;else echo d;fi
     else
      if [ "${b6}" = "0" ];then echo e;else echo f;fi
     fi
    fi
   fi
  fi
 else
  if [ "${b2}" = "0" ];then
   if [ "${b3}" = "0" ];then
    if [ "${b4}" = "0" ];then
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo g;else echo h;fi
     else
      if [ "${b6}" = "0" ];then echo i;else echo j;fi
     fi
    else
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo k;else echo l;fi
     else
      if [ "${b6}" = "0" ];then echo m;else echo n;fi
     fi
    fi
   else
    if [ "${b4}" = "0" ];then
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo o;else echo p;fi
     else
      if [ "${b6}" = "0" ];then echo q;else echo r;fi
     fi
    else
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo s;else echo t;fi
     else
      if [ "${b6}" = "0" ];then echo u;else echo v;fi
     fi
    fi
   fi
  else
   if [ "${b3}" = "0" ];then
    if [ "${b4}" = "0" ];then
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo w;else echo x;fi
     else
      if [ "${b6}" = "0" ];then echo y;else echo z;fi
     fi
    else
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo 0;else echo 1;fi
     else
      if [ "${b6}" = "0" ];then echo 2;else echo 3;fi
     fi
    fi
   else
    if [ "${b4}" = "0" ];then
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo 4;else echo 5;fi
     else
      if [ "${b6}" = "0" ];then echo 6;else echo 7;fi
     fi
    else
     if [ "${b5}" = "0" ];then
      if [ "${b6}" = "0" ];then echo 8;else echo 9;fi
     else
      if [ "${b6}" = "0" ];then echo +;else echo /;fi
     fi
    fi
   fi
  fi
 fi
}

hex2bin()
{
 if   [ "$1" = "0" ];then echo 0000;
 elif [ "$1" = "1" ];then echo 0001;
 elif [ "$1" = "2" ];then echo 0010;
 elif [ "$1" = "3" ];then echo 0011;
 elif [ "$1" = "4" ];then echo 0100;
 elif [ "$1" = "5" ];then echo 0101;
 elif [ "$1" = "6" ];then echo 0110;
 elif [ "$1" = "7" ];then echo 0111;
 elif [ "$1" = "8" ];then echo 1000;
 elif [ "$1" = "9" ];then echo 1001;
 elif [ "$1" = "a" ];then echo 1010;
 elif [ "$1" = "b" ];then echo 1011;
 elif [ "$1" = "c" ];then echo 1100;
 elif [ "$1" = "d" ];then echo 1101;
 elif [ "$1" = "e" ];then echo 1110;
 else                     echo 1111;
 fi
}

six_hex_to_4base64()
{
 local hex1=${1:0:1}
 local hex1bin=$(hex2bin ${hex1})
 local hex2=${1:1:1}
 local hex2bin=$(hex2bin ${hex2})
 local hex3=${1:2:1}
 local hex3bin=$(hex2bin ${hex3})
 local hex4=${1:3:1}
 local hex4bin=$(hex2bin ${hex4})
 local hex5=${1:4:1}
 local hex5bin=$(hex2bin ${hex5})
 local hex6=${1:5:1}
 local hex6bin=$(hex2bin ${hex6})
 local b64_1=""
 local b64_2=""
 local b64_3="="
 local b64_4="="
 if [ "${hex3}" = "" ];then
  b64_1=$(six_bin2base64 "${hex1bin}${hex2bin:0:2}")
  b64_2=$(six_bin2base64 "${hex2bin:2:2}0000")
 elif [ "${hex5}" = "" ];then
  b64_1=$(six_bin2base64 "${hex1bin}${hex2bin:0:2}")
  b64_2=$(six_bin2base64 "${hex2bin:2:2}${hex3bin}")
  b64_3=$(six_bin2base64 "${hex4bin}00")
 else
  b64_1=$(six_bin2base64 "${hex1bin}${hex2bin:0:2}")
  b64_2=$(six_bin2base64 "${hex2bin:2:2}${hex3bin}")
  b64_3=$(six_bin2base64 "${hex4bin}${hex5bin:0:2}")
  b64_4=$(six_bin2base64 "${hex5bin:2:2}${hex6bin}")
 fi
 echo "${b64_1}${b64_2}${b64_3}${b64_4}"
}


 local word=$1
 local b64=""
 while [ "${word}" != "" ];do
  b64+=$(six_hex_to_4base64 ${word:0:6})
  word=${word:6}
 done
 echo ${b64}
}

hex2sid()
{
eight_hex2dec()
{
 local input
 local x
 input=$1
 x=$2
# s=$(printf "%-${x}s" "*")
# echo "${s// /*}${input:$x:8}"
# echo "${s// /*}${input:$(($x+6)):2}${input:$(($x+4)):2}${input:$(($x+2)):2}${input:$(($x+0)):2}"
 printf "$((0x${input:$(($x+6)):2}${input:$(($x+4)):2}${input:$(($x+2)):2}${input:$(($x+0)):2}))"
}

local input
local x
input=$1
#0        1         2         3         4         5
#12345678901234567890123456789012345678901234567890123456
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#^^
printf "S-$((0x${input:0:2}))-"
#0        1         2         3         4         5
#12345678901234567890123456789012345678901234567890123456
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#  ^^
printf "$((0x${input:2:2}))-"
#0        1         2         3         4         5
#12345678901234567890123456789012345678901234567890123456
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                      ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                    ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                  ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                ^^
x=16
eight_hex2dec "${input}" "${x}"
printf "-"
#0        1         2         3         4         5
#12345678901234567890123456789012345678901234567890123456
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                              ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                            ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                          ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                        ^^
x=$(($x+8))
eight_hex2dec "${input}" "${x}"
printf "-"
#0        1         2         3         4         5
#12345678901234567890123456789012345678901234567890123456
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                      ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                    ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                  ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                ^^
x=$(($x+8))
eight_hex2dec "${input}" "${x}"
printf "-"
#0        1         2         3         4         5
#12345678901234567890123456789012345678901234567890123456
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                              ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                            ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                          ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                        ^^
x=$(($x+8))
eight_hex2dec "${input}" "${x}"
printf "-"
#0        1         2         3         4         5
#12345678901234567890123456789012345678901234567890123456
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                                      ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                                    ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                                  ^^
#010500000000000515000000A065CF7E784B9B5FE77C8770091C0100
#                                                ^^
x=$(($x+8))
eight_hex2dec "${input}" "${x}"
echo
}

sid2hex()
{
dec2_8hex()
{
 local b
 b=$(printf "%08x" "$1")
 echo -n "${b:6:2}${b:4:2}${b:2:2}${b:0:2}"
}

 local partn
 partn=0
 for part in $(echo "$1" | awk '{split($1,sidArr,"-")} END { for (sidP in sidArr) {print sidArr[sidP]} }')
 do
  partn=$((${partn}+1))
  if [ $partn = 2 ];then
   printf "%02x" ${part}
  else
   if [ $partn = 3 ];then
    printf "%02x" ${part}
    printf "%012x" ${part}
   else
    if [ $partn != 1 ];then
     dec2_8hex ${part}
    fi
   fi
  fi
 done
 echo
}

base64toSid()
{
 hex2sid $(base64toHex $1) 
}

sid2base64()
{
 hex2base64 $(sid2hex $1)
}

base64toGuid()
{
eightLittle2big()
{
 local l
 l=$1
 echo "${l:6:2}${l:4:2}${l:2:2}${l:0:2}"
}

fourLittle2big()
{
 local l
 l=$1
 echo "${l:2:2}${l:0:2}"
}

 local l
 l=$(base64toHex $1)
 echo "$(eightLittle2big ${l:0:8})-$(fourLittle2big ${l:8:4})-$(fourLittle2big ${l:12:4})-${l:16:4}-${l:20:12}"
}
